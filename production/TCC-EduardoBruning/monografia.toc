\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{15}{chapter.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{16}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos}{16}{section.1.2}
\contentsline {section}{\numberline {1.3}Metodologia de Trabalho}{17}{section.1.3}
\contentsline {section}{\numberline {1.4}Organiza\IeC {\c c}\IeC {\~a}o do Restante do Trabalho}{17}{section.1.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}Ferramentas \ac {case}}{19}{section.2.1}
\contentsline {section}{\numberline {2.2}Linguagem \ac {uml}}{20}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Diagramas}{21}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Modelagem de Software}{21}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Modelagem Individual}{23}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Modelagem Colaborativa}{23}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Comunica\IeC {\c c}\IeC {\~a}o de Aplica\IeC {\c c}\IeC {\~o}es}{24}{section.2.4}
\contentsline {section}{\numberline {2.5}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{25}{section.2.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {M\IeC {\'e}todos e Recursos}}{27}{chapter.3}
\contentsline {section}{\numberline {3.1}Pr\IeC {\'a}ticas de Engenharia de \textit {Software}}{27}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Processo Unificado}{27}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}\textit {StoryBoard}}{29}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Hist\IeC {\'o}rias de Usu\IeC {\'a}rios}{30}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Arquitetura de sistemas distribu\IeC {\'\i }dos}{30}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Arquiteturas \textit {peer-to-peer}}{32}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Plataforma JXTA}{34}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Arquitetura JXTA}{34}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{35}{section.3.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Trabalhos Relacionados}}{37}{chapter.4}
\contentsline {section}{\numberline {4.1}Metodologia}{37}{section.4.1}
\contentsline {section}{\numberline {4.2}Resultado}{38}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Quais s\IeC {\~a}o as plataformas onde as atuais solu\IeC {\c c}\IeC {\~o}es rodam?}{38}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Quais s\IeC {\~a}o as tecnologias que as solu\IeC {\c c}\IeC {\~o}es utilizam?}{39}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Que estrat\IeC {\'e}gias usadas para tratar a edi\IeC {\c c}\IeC {\~a}o colaborativa?}{40}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{41}{section.4.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Desenvolvimento Preliminar}}{43}{chapter.5}
\contentsline {section}{\numberline {5.1}Fase de Concep\IeC {\c c}\IeC {\~a}o}{43}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Descri\IeC {\c c}\IeC {\~a}o do Problema}{43}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Vis\IeC {\~a}o Geral}{44}{subsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.2.1}\textit {StoryBoard}}{44}{subsubsection.5.1.2.1}
\contentsline {subsubsection}{\numberline {5.1.2.2}Hist\IeC {\'o}rias de Usu\IeC {\'a}rios}{45}{subsubsection.5.1.2.2}
\contentsline {section}{\numberline {5.2}Fase de Elabora\IeC {\c c}\IeC {\~a}o - Itera\IeC {\c c}\IeC {\~a}o 1}{46}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Arquitetura da Ferramenta}{46}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Modelo de Dom\IeC {\'\i }nio da Ferramenta}{47}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{48}{section.5.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Conclus\IeC {\~o}es Preliminares}}{49}{chapter.6}
\contentsline {section}{\numberline {6.1}Cronograma}{49}{section.6.1}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{51}{section*.7}
