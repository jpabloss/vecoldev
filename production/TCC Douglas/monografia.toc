\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{15}{chapter.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{15}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivo}{16}{section.1.2}
\contentsline {section}{\numberline {1.3}Metodologia}{17}{section.1.3}
\contentsline {section}{\numberline {1.4}Organiza\IeC {\c c}\IeC {\~a}o do Trabalho}{17}{section.1.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}Linguagem Unificada de Modelagem}{19}{section.2.1}
\contentsline {subsubsection}{\numberline {2.1.0.1}Diagramas Estruturais}{20}{subsubsection.2.1.0.1}
\contentsline {subsubsection}{\numberline {2.1.0.2}Diagramas Comportamentais}{21}{subsubsection.2.1.0.2}
\contentsline {section}{\numberline {2.2}Ferramentas \ac {case}}{22}{section.2.2}
\contentsline {section}{\numberline {2.3}Processamento de Imagens Digitais}{22}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Aquisi\IeC {\c c}\IeC {\~a}o de imagens}{25}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Filtragem, realce e restaura\IeC {\c c}\IeC {\~a}o de imagens}{25}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Processamento de imagens coloridas}{26}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Compress\IeC {\~a}o de imagens}{27}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}An\IeC {\'a}lise de Imagens Digitais}{28}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Segmenta\IeC {\c c}\IeC {\~a}o de Imagens}{28}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Representa\IeC {\c c}\IeC {\~a}o e Descri\IeC {\c c}\IeC {\~a}o}{28}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Morfologia Matem\IeC {\'a}tica}{29}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Classifica\IeC {\c c}\IeC {\~a}o de Padr\IeC {\~o}es}{29}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{30}{section.2.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {M\IeC {\'e}todos e Recursos}}{31}{chapter.3}
\contentsline {section}{\numberline {3.1}Pr\IeC {\'a}ticas de Engenharia de Software}{31}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Processo Unificado (\ac {pu})}{31}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Storyboard}{32}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Hist\IeC {\'o}rias de Usu\IeC {\'a}rio}{33}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Plataforma Android}{33}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Ambiente de Desenvolvimento}{34}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Vers\IeC {\~a}o do Android}{34}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Alguns Recursos B\IeC {\'a}sicos}{34}{subsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.3.1}Activity}{34}{subsubsection.3.2.3.1}
\contentsline {subsubsection}{\numberline {3.2.3.2}View}{34}{subsubsection.3.2.3.2}
\contentsline {subsubsection}{\numberline {3.2.3.3}Consumo de Servi\IeC {\c c}os Web}{35}{subsubsection.3.2.3.3}
\contentsline {section}{\numberline {3.3}\ac {opencv}}{36}{section.3.3}
\contentsline {section}{\numberline {3.4}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{37}{section.3.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Trabalhos Relacionados}}{39}{chapter.4}
\contentsline {section}{\numberline {4.1}Metodologia}{39}{section.4.1}
\contentsline {section}{\numberline {4.2}Resultados}{40}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Quais t\IeC {\'e}cnicas de aquisi\IeC {\c c}\IeC {\~a}o de imagem foram utilizadas}{41}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Quais filtros foram utilizados}{42}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Quais t\IeC {\'e}cnicas de segmenta\IeC {\c c}\IeC {\~a}o de imagem foram utilizadas}{42}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Quais t\IeC {\'e}cnicas de reconhecimento de padr\IeC {\~o}es foram utilizadas}{43}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{45}{section.4.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Desenvolvimento Preliminar}}{47}{chapter.5}
\contentsline {section}{\numberline {5.1}Fase de Concep\IeC {\c c}\IeC {\~a}o}{47}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Vis\IeC {\~a}o geral do problema}{47}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Vis\IeC {\~a}o da Solu\IeC {\c c}\IeC {\~a}o}{47}{subsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.2.1}Hist\IeC {\'o}rias de Usu\IeC {\'a}rio}{50}{subsubsection.5.1.2.1}
\contentsline {subsubsection}{\numberline {5.1.2.2}Vis\IeC {\~a}o do Produto}{50}{subsubsection.5.1.2.2}
\contentsline {section}{\numberline {5.2}Fase de Elabora\IeC {\c c}\IeC {\~a}o: Itera\IeC {\c c}\IeC {\~a}o 1}{50}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Modelo de Dom\IeC {\'\i }nio}{51}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Arquitetura}{51}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}Arquitetura Cliente}{52}{subsubsection.5.2.2.1}
\contentsline {subsubsection}{\numberline {5.2.2.2}Arquitetura Servidor}{53}{subsubsection.5.2.2.2}
\contentsline {subsection}{\numberline {5.2.3}Processo de Reconhecimento}{53}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{54}{section.5.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Conclus\IeC {\~o}es Preliminares}}{55}{chapter.6}
\contentsline {section}{\numberline {6.1}Cronograma}{55}{section.6.1}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{57}{section*.7}
