%==============================================================================
\chapter{Fundamentação Teórica}\label{fundamentacaoTeorica}
%==============================================================================
Neste capítulo apresentamos os conceitos ligados ao nosso trabalho. Abordamos conceitos relacionados com os sistemas de reconhecimento de esboços e modelagem de software. Na \autoref{uml} apresentamos a \ac{UML}, uma linguagem de modelagem de software. Na \autoref{ferramentasCase} apresentamos os recursos computacionais que ajudam os engenheiros de software nas etapas de construção de um software. Na \autoref{processamentoImagens} apresentamos a área que é responsável pela melhoria das características de uma imagem ou retirada de características indesejadas. Por fim na \autoref{analiseImagens} apresentamos a área que visa a compreensão do conteúdo de uma imagem.

\section{Linguagem Unificada de Modelagem}\label{uml}
A \ac{UML} é uma linguagem de modelagem de software padrão que pode ser empregada na visualização, especificação, construção e documentação de artefatos de software. Foi criada por Grady Booch, James Rumbaugh e Ivar Jacobson com objetivo de padronizar a forma de modelar software.

A \ac{UML} além de oferecer recursos para modelagem de um software, também pode ser aplicada por exemplo na modelagem do fluxo de trabalho de uma empresa\cite{booch2000uml}.

A \ac{UML} foi criada com objetivo de oferecer uma única linguagem de modelagem após o aparecimento de várias linguagens orientadas a objetos (OO). Segundo \citeonline{booch2000uml} durante o período de 1989 e 1994 haviam em torno de 50 métodos para análise e projeto OO. Os usuários dos métodos de análise e projeto OO tinham dificuldade de encontrar um método que atendesse a todas suas necessidades.  Depois dessa experiência novos métodos foram criados, alguns com maior destaque como Booch, o \ac{oose} de Jacobson e \ac{omt} de Rumbaugh. Cada um desses métodos possuía pontos forte e pontos fracos. Como os métodos dos três autores estavam indo em uma direção parecida, os mesmos foram motivados a criar uma linguagem unificada. Em 1994 iniciaram os esforços para a criação da \ac{UML}. Em 1995 foi lançado a versão 0.8 da \ac{UML} que na época era chamada de método unificado. A documentação da versão 0.9 foi lançada em 1996 após Jacobson se associar a Rational e iniciar a incorporação do método \ac{oose}. Em 1997 a versão 1.1 da \ac{UML} foi adotada pela \ac{omg}. Atualmente a \ac{UML} está na versão 2.4.1 e a versão 2.5 está em fase beta.

A \ac{UML} possui três tipos de blocos de construção denominados de itens, relacionamentos e diagramas. Os itens podem ser divididos em estruturais, comportamentais, de agrupamento e notacionais. Os diagramas da \ac{UML} 2.5 podem ser divididos em duas categorias denominadas de diagramas estruturais e diagramas comportamentais mostrados na \autoref{fig:estruturaDiagramasUML}. 

Cada um dos diagramas podem ter modelos derivados de sua estrutura. Um exemplo de diagrama que possui alguns modelos derivados é o diagrama de classes, no qual podemos citar como derivado o modelo de domínio. Na construção de um modelo de domínio não é levado em conta questões tecnológicas e sim apenas elementos do negócio no qual a aplicação faz parte.

\begin{figure}[h]
	\caption{Diagramas da \ac{UML} 2.5 \cite{uml25}.}
	\label{fig:estruturaDiagramasUML}
	\centering
	\includegraphics[height=12cm]{diagramasUML}
\end{figure}
 
 Nas próximas seções vamos mostrar os diagramas que compõem cada uma das categorias. Na \autoref{diagramasestruturais} apresentamos os diagramas estruturais. Já na \autoref{diagramascomportamentais} apresentamos os diagramas comportamentais.
 \subsubsection{Diagramas Estruturais}\label{diagramasestruturais}
 Os diagramas estruturais tem como objetivo mostrar o sistema de uma perspectiva estática. Abaixo podemos ver uma pequena descrição de cada um deles.
 \begin{itemize}
 	\item \textbf{Diagrama de classes:} mostra classes, interfaces, colaborações e seus relacionamentos. As classes são representações de um conjunto de objetos. As interfaces são um conjunto de operações que são utilizadas para especificar os serviços que uma classe ou componente vai oferecer. Já as colaborações são um conjunto de classes, interfaces ou qualquer outro elemento onde todos eles estão relacionados \cite{booch2000uml}.
 	\item \textbf{Diagrama de objetos:} mostra as instâncias de cada classe, os valores dos atributos dos objetos e seus relacionamentos \cite{uml25}. 
 	\item \textbf{Diagrama de pacotes:} além de mostrar os pacotes do sistema, também pode ser utilizado para mostrar alguma abstração ou visão específica de um sistema, para descrever aspectos arquitetônicos, lógicas ou comportamentos do sistema \cite{uml25}.
 	\item \textbf{Diagrama de estrutura composta:} mostra a estrutura interna de uma classe ou colaboração \cite{booch2000uml}.
 	\item \textbf{Diagrama de componentes:}  mostra as partes internas, os conectores e as portas que implementam um componente \cite{booch2000uml}.
 	\item \textbf{Diagrama de implantação:} mostra um conjunto de nós relacionados sendo utilizado para visualizar a implantação de uma arquitetura em uma perspectiva estática \cite{booch2000uml}. Também pode ser utilizado para visualizar a arquitetura lógica e física de uma rede \cite{uml25}.
 	\item \textbf{Diagrama de perfil:} um perfil possibilita a modificação da UML para diferentes plataformas ou domínios \cite{uml25}, possibilitando criar uma extensão leve da UML.
 \end{itemize}
 
 \subsubsection{Diagramas Comportamentais}\label{diagramascomportamentais}
 Os diagramas comportamentais tem como objetivo mostrar o sistema em uma perspectiva mais dinâmica. Abaixo podemos ver uma pequena descrição de cada um deles.
 \begin{itemize}
 	\item \textbf{Diagrama de caso de uso:} mostra um conjunto de ações feitas por atores em um sistema. As ações podem ser feitas em colaboração com um ou mais usuários externos do sistema \cite{uml25}.
 	\item \textbf{Diagrama de atividade:} mostra o fluxo de atividades do sistema onde cada atividade pode ter seu fluxo alterado por uma condição. Em um diagrama de atividade também pode ser apresentado as ações ou modificações de um objeto durante o fluxo de atividades \cite{booch2000uml}.
 	\item \textbf{Diagrama de máquina de estado:} mostra um conjunto de estado, suas transições, eventos e atividades. São importantes para modelagem do comportamento de uma interface, classe ou colaboração \cite{booch2000uml}.
 	\item \textbf{Diagrama de sequência:} mostra uma sequência de mensagens enviadas ou recebidas entre um conjunto de instâncias. Este diagrama tem ênfase na organização temporal das mensagens \cite{booch2000uml}.
 	\item \textbf{Diagrama de comunicação:} mostra um conjunto de instâncias e suas conexões dando ênfase à organização estrutural dos objetos \cite{booch2000uml}.
 	\item \textbf{Diagrama temporal:} mostra interações quando um objetivo principal do diagrama trabalha em cima de um determinado tempo. Diagramas temporais se concentram em condições de mudança dentro e entre as linhas de vida de um objeto\cite{uml25}. 
 	\item \textbf{Diagrama de visão geral de interação} é uma variante do diagrama de atividades, apresentando uma visão geral do fluxo de controle \cite{uml25}.
 \end{itemize} 

\section{Ferramentas \ac{case}}\label{ferramentasCase}
As ferramentas computacionais que auxiliam os engenheiros de softwares no desenvolvimento são denominadas de (\ac{case}) \citeonline{sommerville2003engenharia}. Seu principal objetivo é automatizar algumas das etapas do desenvolvimento de software. Exemplos de ferramentas \ac{case} são ambientes de desenvolvimento, ferramentas de gerenciamento de equipe, ferramentas de modelagem etc.  

\citeonline{sommerville2003engenharia} apresenta a classificação para as ferramentas \ac{case} em uma perspectiva funcional, de processo e de integração. Na perspectiva funcional as ferramentas \ac{case} são classificadas a partir de sua função como por exemplo ferramentas de planejamento, de edição, de testes etc. Na perspectiva de processo as ferramentas são classificadas a partir de que atividades são apoiadas com seu uso. Como por exemplo ferramentas de edição são utilizadas na especificação, projeto, implementação e verificação e validação. Já ferramentas de teste são utilizadas na implementação e verificação e validação. Por ultimo \apudonline{sommerville2003engenharia}{fuggetta1993classification} classifica as ferramentas \ac{case} por abrangência do processo como mostra a \autoref{fig:categoriaCase}.
\begin{figure}[h]
	\caption{Classificação ferramentas \ac{case} por abrangência \apud{sommerville2003engenharia}{fuggetta1993classification}}
	\label{fig:categoriaCase}
	\centering
	\includegraphics[width=\textwidth]{classificacaoFerramentasCase}
\end{figure}


\section{Processamento de Imagens Digitais}\label{processamentoImagens}
%[P1. O que é o processamento de imagens]%
O processamento de imagens consiste em um conjunto de técnicas para capturar, representar e transformar imagens com auxílio de computador \cite{pedrini2008analise}. O interesse nos métodos de processamento digital de imagens vem de duas áreas principais de aplicação: melhora das informações visuais para interpretação humana e processamento de dados de imagens para armazenamento, transmissão e representação, considerado a percepção automática por máquinas \cite{gonzalez2010processamento}. 

A visão do ser humano é bastante limitada à banda visual do \ac{em} mostrada na \autoref{fig:espectro_eletromagnetico}. Os aparelhos de processamento de imagens cobrem quase todo o espectro \ac{em}, possibilitando gerar imagens digitais de fontes incomuns ao olho humano como ultrassom, microscopia eletrônica e imagens geradas por computador \cite{gonzalez2010processamento}.
\begin{figure}[h]
	\caption{Espectro eletromagnético \cite{gonzalez2010processamento}.}
	\label{fig:espectro_eletromagnetico}
	\centering
	\includegraphics[width=\textwidth]{espectro_eletromagnetico.png}
\end{figure}

%[P2. O que é uma imagem Digital]%
Basicamente podemos definir uma imagem digital como uma função bidimensional onde em cada par de coordenadas (x,y) possui um valor representando a densidade, também chamado nível de cinza. Este par de coordenada possui vários nomes porém o mais utilizado é Pixel.

%[P3. Ligação do processamento digital de imagens com análise de imagens e visão computacional]%
As fronteiras entre processamento de imagens, análise de imagens e visão computacional não são totalmente divididas e não há um acordo geral sobre isso. Para tentar dividir ou criar uma fronteira artificial é possível levar em consideração três tipos de processos computacionais: baixo, médio e alto. Processos de nível baixo entram em várias operações onde a entrada é uma imagem e a saída também. Os processos de nível médio levam em conta a extração de características das imagens. Já as de nível alto tentam dar sentido a imagem. O processamento de imagens está em um nível baixo e a análise estaria em um nível médio e alto. A visão computacional está localizada no nível alto também, juntamente com a análise de imagens \cite{gonzalez2010processamento}.

%[P4. Componentes de um software de processamento de imagens]%
Os componentes básicos de um sistema de processamento de imagens de uso geral apresentado por \citeonline{gonzalez2010processamento} podem ser dividido conforme mostrado na \autoref{fig:componentes_sistema}. Os monitores são utilizados na apresentação da imagem e podem ser monitores de TV onde raramente é necessário placas de vídeos complexas. O computador pode ser de uso geral, onde em alguns casos é necessário um computador especializado para aumento de desempenho. O armazenamento em massa é feito por um dispositivo onde é indispensável para grandes quantidades de imagens. O sistema de registro são dispositivos para impressão ou apresentação da imagem. O hardware especializado em processamento de imagens normalmente consiste em um digitalizador (conversor de estímulos elétricos em dados digitais) e um hardware para realizar operações que requerem um rápido processamento. O Software de processamento de imagens são módulos especializados para a realização de tarefas especificas. Os sensores são os dispositivos responsáveis pela aquisição da imagem. Por ultimo temos a rede de comunicação que faz a transferência de dados entre os componentes. 
\begin{figure}[h]
	\caption{Componentes de um software de processamento de imagens de uso geral \cite{gonzalez2010processamento}.}
	\label{fig:componentes_sistema}
	\centering
	\includegraphics[height=11cm]{componentesSoftwareProcessamentoImagens}
\end{figure}

%[P5. Passos fundamentais para o processamento de imagens]%
Segundo \citeonline{gonzalez2010processamento} alguns passos são fundamentais no processamento digital de imagens. Na \autoref{fig:passos_fundamentais} podemos ver alguns métodos utilizados no processamento de imagens que são aplicados com diferentes propósitos e objetivos. Podemos considerar como métodos de processamento de imagem a aquisição da imagem, realce de imagens, restauração de imagens, processamento de imagens coloridas e a compreensão de imagens. Os métodos de processamento morfológico, segmentação, representação e descrição e reconhecimento de objetos (classificação de padrões) são métodos de análise de imagens e serão descritos melhor na \autoref{analiseImagens}.
\begin{figure}[h]
	\caption{Passos fundamentais para o processamento de imagens}
	\label{fig:passos_fundamentais}
	\centering
	\includegraphics[height=11cm]{passosFundamentaisProcessamentoImagens}
\end{figure}

\subsection{Aquisição de imagens}
A aquisição de uma imagem pode ser feita de várias formas: com um único sensor, por varredura de linha ou com sensores matriciais. Para ser possível criar uma imagem digital é necessário converter os dados contínuos capturados pelos sensores. O processo de conversão envolve duas etapas: amostragem e a quantização. A amostragem seleciona partes de uma cadeia de dados capturada pelos sensores, e a quantização define qual a intensidade vai ser a intensidade de cada amostra. A figura \ref{fig:amostragem_quantizacao} exemplifica melhor o processo de amostragem e quantização, onde os pontos vermelhos são as amostras escolhidas e o quadrado preenchido no lado direito é a intensidade que cada amostra vai ter como valor  \cite{gonzalez2010processamento}.
\begin{figure}[h]
	\caption{Amostragem e quantização (adaptada de \citeonline{gonzalez2010processamento}).}
	\label{fig:amostragem_quantizacao}
	\centering
	\includegraphics[width=\textwidth]{amostragemQuantizacao}
\end{figure}

\subsection{Filtragem, realce e restauração de imagens}
O realce da imagem é um processo que tem como objetivo melhorar as características da imagem, onde sua aplicação depende totalmente do domínio do problema. As técnicas de realce de imagens possuem um observador para decidirem se o resultado foi o esperado  \cite{gonzalez2010processamento}. O realce é necessário quando a imagem possui alguma degradação, perda de qualidade, perda de contraste, borramento, distorção, ou condição inadequada de iluminação \cite{pedrini2008analise}.

A restauração de imagens utiliza modelos matemáticos ou probabilísticos. Em relação com o realce de imagens a restauração de imagens é mais objetivo, visto que o realce de imagens se baseia na preferência humana \cite{gonzalez2010processamento}.

\subsection{Processamento de imagens coloridas}
O processamento de imagens coloridas é diferente do processamento de imagens em níveis de cinza ou binárias. Está área tem um grande destaque visto o aumento na utilização de imagens digitais na internet \cite{gonzalez2010processamento}. A utilização de imagens coloridas no processamento de imagens se da segundo  \citeonline{gonzalez2010processamento}  a dois fatores principais, onde o primeiro é que as cores facilitam a identificação e extração de objetos e o segundo é que os seres humanos são capazes de ver uma quantidade muito maior de tons em relação a imagens de tons de cinza, facilitando a análise manual.

Visto que existem milhões de tons de cores, existe modelos para especificar as cores em formatos padrões. Esses modelos existem para atender diferentes dispositivos ou aplicações que utilizam cores em seus processos. Segundo \cite{pedrini2008analise} um modelo de cor é essencialmente uma representação tridimensional onde cada cor é especificada por um ponto no sistemas de coordenadas tridimensionais. Alguns exemplos de modelos de cores são o modelo de Munsell, RGB, XYZ, CMY, YIQ, YUV, YCbCr, HSV, HSL, HSI.

O processamento digital de imagens coloridas pode ser divididos em duas categorias principais. A primeira categoria consiste em processar os componentes da imagem digital separados e depois gerar a imagem colorida a partir deles. O segundo consiste em trabalhar diretamente com os pixels coloridos  \cite{gonzalez2010processamento}.

\subsection{Compressão de imagens}
A compreensão de imagens é o processo ao qual são utilizadas técnicas para redução do tamanho de armazenamento de uma imagem ou também largura de banda para transmiti-la  \cite{gonzalez2010processamento}. 

As técnicas de compressão de imagens podem ser divididas em duas categorias denominadas de compressão sem perda e com perda. A compressão sem perdas é quando a imagem descompactada não perdeu nenhuma informação. Este tipo de compressão é utilizada quando todas informações da imagem são de extrema importância. A compressão com perdas é quando algumas informações não são recuperadas ao descompactar a imagem \cite{pedrini2008analise}.

A compressão da imagem é baseada na redução de suas redundâncias. As redundâncias são divididas em redundância de codificação, interpixel (redundância espacial e temporal) e psicovisual (informações irrelevantes)\cite{gonzalez2010processamento, pedrini2008analise}. A redundância de codificação ocorre devido ao número de bits que nem sempre são necessários para representar intensidades. A redundância interpixel ocorre porque normalmente os pixels vizinhos de um pixel qualquer possuem informações replicadas. A redundância psicovisual ocorre porque algumas informações visuais são irrelevantes para o olho humano.

Atualmente existem vários padrões para compressão de imagens. Para imagens estáticas binárias podemos citar como exemplo o JBIG. Para imagens estáticas de tons contínuos podemos citar o JPEG. Para compressão de vídeos temos como exemplo MPEG-4. Para imagens estáticas de tons contínuos podemos citar o PDF, PNG e também GIF. 

\section{Análise de Imagens Digitais}\label{analiseImagens}
%[P1. O que é análise de imagens]%
A análise de imagens é, tipicamente, baseada na forma, na textura, nos níveis de cinza ou nas cores dos objetos presentes em uma imagem. A análise de imagens tem como foco compreender o conteúdo da imagem. Algumas técnicas comuns na análise da imagem são a segmentação da imagem em regiões ou objetos de interesse, descrição dos objetos e reconhecimento ou classificação dos mesmos \cite{pedrini2008analise}.

Nas próximas seções vamos apresentar etapas que muitas vezes um software de análise de imagem possui para a compreensão da imagem. Na \autoref{analiseSegmentacao} apresentamos um pouco sobre os conceitos relacionados com a segmentação da imagem. Na \autoref{representacaodescricao} apresentamos uma base sobre a etapa de representação e descrição dos elementos segmentados em uma imagem. Na \autoref{morfologiamatematica} apresentamos os fundamentos da morfologia matemática. Por fim na \autoref{classificacaopadroes} apresentamos a etapa de classificação dos padrões.

\subsection{Segmentação de Imagens}\label{analiseSegmentacao}
%[P2. O que é segmentação]%
A segmentação é um processo importante para identificação de objetos. A imagem é subdividida em regiões ou objetos de interesse. A segmentação é uma das tarefas mais difíceis em um sistema de análise ou processamento de imagens onde a precisão da segmentação determina o sucesso ou o fracasso final de um procedimento \cite{gonzalez2010processamento}. A divisão dos objetos e regiões também depende muito do processamento da imagem onde por exemplo imagens com muito ruído podem distorcer a extração dos dados da imagem \cite{pedrini2008analise}. As técnicas de segmentação buscam detectar descontinuidades e similaridades da imagem. Alguns exemplos de abordagens são a detecção de retas, detecção de bordas, segmentação de regiões e limiarização.
                               
\subsection{Representação e Descrição}\label{representacaodescricao}     
%[P3. O que é representação e decrição]%
As regiões e objetos gerados da segmentação necessitam ser representados e descritos para os próximos passos da análise da imagem. O objeto pode ser representado em termos de suas características externas (bordas) e características internas (pixels).  A descrição depende da representação escolhida e é necessário a extração de características e medidas minimas não permitindo ambiguidades. Exemplos de representação são código de cadeia e seguidor de fronteira. Um exemplo de descritor é o comprimento de uma fronteira.

\subsection{Morfologia Matemática}\label{morfologiamatematica}
%[P4. O que é morfologia matemática]%
A morfologia matemática é uma ferramenta para extrair componentes das imagens que são úteis na representação e na descrição da forma de uma  região \cite{gonzalez2010processamento}. Pode ser utilizada em extração de componentes conexos, busca de padrões específicos na imagem, delimitação do fecho convexo, extração de bordas dos objetos, afinamento de bodas e muitas outras aplicações \cite{pedrini2008analise}. As formas dos objetos são representadas utilizando teoria dos conjuntos. Com a utilização da morfologia matemática podem ser feitas operações como erosão e dilatação de objetos e abertura e fechamento de objetos além de muitas outras operações. 

\subsection{Classificação de Padrões}\label{classificacaopadroes}
%[P5. O que é classificação de padrões]%
A classificação de padrões ou também chamada de reconhecimento de objetos visa mapear amostras extraídas com um conjunto de rótulos possuindo algumas restrições para mapear amostras com características semelhantes \cite{pedrini2008analise}. Por exemplo a figura \ref{fig:reconhecimento_padroes} seria classificada contendo triângulos, círculos e quadrados. Apesar de ter círculos de vários tamanhos, os mesmos possuem características semelhantes. Um exemplo de abordagem que pode ser utilizada para classificação de padrões são redes neurais.
\begin{figure}[h]
	\caption{Figuras geométricas que devem ser classificadas com relação a sua forma.}
	\label{fig:reconhecimento_padroes}
	\centering
	\includegraphics[width=8cm]{reconhecimentoPadroes}
\end{figure}

\section{Considerações do Capítulo}\label{conclusaoFundamentacaoTeorica}
Como foi possível ver, apresentamos os conceitos relacionados com nosso trabalho. As ferramentas \ac{case} para modelagem \ac{UML} atualmente apresentam vários recursos para modelagem de diversos diagramas. Porém poucas oferecem recursos além da modelagem \ac{UML}. 

A \ac{UML} como uma linguagem unificada possui uma grande game de diagrama. Em sua versão atual temos 14 diagramas diferentes para serem utilizados na modelagem de um software.

Agora enfocando no processamento e análise de imagens. Foi possível perceber que as duas áreas trabalham juntas na resolução de problemas de interpretação de imagens. Apesar de não focarmos muito nos algoritmos que existem, o objetivo de apresentar os principais conceitos sobre as duas áreas foi alcançado.



